package com.poli.eventos.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.poli.eventos.model.entities.Artista_techno;
import com.poli.eventos.model.entities.Usuario_techno;

//import com.poli.devweb.model.entities.User;

@Repository
public interface ArtistaRepository extends CrudRepository<Artista_techno, Long> {
	
//	public User findById(long id);
	
	//public List<User> findByEmail(String email);
}
