package com.poli.eventos.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.poli.eventos.model.entities.Usuario_techno;


@Repository
public interface UsuarioRepository extends CrudRepository<Usuario_techno, Long> {
	public Usuario_techno findByEmailAndContraseña(String email, String contraseña);
	public Usuario_techno findByNombreAndContraseña(String nombre, String contraseña);
}
