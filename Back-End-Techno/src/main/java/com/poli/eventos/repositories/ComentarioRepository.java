package com.poli.eventos.repositories;

import java.util.List;

import org.jboss.logging.Param;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.poli.eventos.model.entities.Comentario_techno;
import com.poli.eventos.model.entities.Usuario_techno;

//import com.poli.devweb.model.entities.User;

@Repository
public interface ComentarioRepository extends CrudRepository<Comentario_techno, Long> {
	
//	public User findById(long id);

    @Async
    @Query("SELECT c FROM Comentario_techno c where c.artista.idArtista = :id") 
	public List<Comentario_techno> findByIdArtista_techno( Long id);
}
