package com.poli.eventos.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.poli.eventos.model.entities.Artista_techno;
import com.poli.eventos.model.entities.Usuario_techno;
import com.poli.eventos.repositories.ArtistaRepository;
import com.poli.eventos.repositories.UsuarioRepository;

@RestController
public class publicacionesController {
	
	@Autowired
	private ArtistaRepository ArtistaRepositoryDAO;
	
	@RequestMapping("/obtenerArtistas")
	public Iterable<Artista_techno> getAllUsers () {
		
		Iterable<Artista_techno> findAll = ArtistaRepositoryDAO.findAll();
		
		return findAll;
		
	}
	
	@RequestMapping(value="/añadirArtista", method = RequestMethod.GET)
    public String añadirArtista(@RequestBody Artista_techno artista) {//REST Endpoint.
		ArtistaRepositoryDAO.save(artista);
    	return"Artista Creado Exitosamente";
    }
}
