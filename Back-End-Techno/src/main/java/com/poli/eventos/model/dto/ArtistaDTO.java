package com.poli.eventos.model.dto;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import com.poli.eventos.model.entities.Artista_techno;
import com.poli.eventos.model.entities.Comentario_techno;

public class ArtistaDTO {
	
	private String nombre;
	//private String titulo;
	private Long id;
    //private String imagenArtista;
    private String descripcion;
    

	
    public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ArtistaDTO(Long id,String nombreAritsa, String titulo, String imagenArtista, String descripcionArtista  		) {
    	this.id=id;
		this.nombre = nombreAritsa;
		/*this.Titulo = titulo;
		this.ImagenArtista = imagenArtista;*/
		this.descripcion = descripcionArtista;
		
	}

	public static Set<ArtistaDTO> Artista_technoAArtistaDTO(Iterable<Artista_techno> Artista) {
		Set<ArtistaDTO> pDTO= new HashSet<ArtistaDTO>();
		for(Artista_techno A: Artista) {
			ArtistaDTO artistaDTO=new ArtistaDTO(A.getIdArtista(),A.getNombre(),A.getTitulo_perfil(),A.getRutaImagen(),A.getDescripcion());
			pDTO.add(artistaDTO);
		}
		
		return pDTO;
	}
	
}