package com.poli.eventos.model.dto;

public class UsuarioDTO {
	private String nombre;
	private String clave;
	
	public UsuarioDTO() {
		
	}
	
	public UsuarioDTO(String nombre,String contraseña) {
		this.nombre=nombre;
		this.clave=contraseña;

	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}




	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}



}
