package com.poli.eventos.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;



@Entity
@Table(name="usuario_techno")

public class Usuario_techno {

	
@Id
@Column(name="Id_UsuarioTechno")
@GeneratedValue(strategy=GenerationType.AUTO)
private Long IdUsuarioTechno;
private String nombre;
private String	email;
private String	contraseña;


@OneToMany(mappedBy="usuario")
@JsonManagedReference
private Set<Comentario_techno> comentarios =new HashSet<Comentario_techno>(0); 


public Long getIdUsuarioTechno() {
	return IdUsuarioTechno;
}
public void setIdUsuarioTechno(Long IdUsuarioTechno) {
	this.IdUsuarioTechno = IdUsuarioTechno;
}

public Usuario_techno() {
	// TODO Auto-generated constructor stub
}
public Usuario_techno(String nombre, String email, String contraseña
		//, Perfil_techno perfil
	//Set<Comentario_techno> comentarios
		) 
{
	this.nombre = nombre;
	this.email = email;
	this.contraseña = contraseña;
	//this.Perfil = perfil;
	//this.comentarios = comentarios;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getContraseña() {
	return contraseña;
}
public void setContraseña(String contraseña) {
	this.contraseña = contraseña;
}
public Set<Comentario_techno> getComentarios() {
	return comentarios;
}
public void setComentarios(Set<Comentario_techno> comentarios) {
	this.comentarios = comentarios;
}

}

