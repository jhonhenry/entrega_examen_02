package com.poli.eventos.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.annotation.JsonBackReference;



@Entity
@Table(name="cometarios_techno")
public class Comentario_techno {
	
	@Id
	@Column(name="Id_Cometario")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long IdCometario;
	
	private String Descripcion;

	
	@ManyToOne 
	@JsonBackReference
	@JoinColumn(name="Id_Usuariotechno")
	private Usuario_techno usuario = new Usuario_techno();
	
	
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name="id_Artista")
	private Artista_techno artista;
	
	public long getIdCometario() {
		return IdCometario;
	}
	public void setIdCometario(long idCometario) {
		IdCometario = idCometario;
	}
	
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	
	public Comentario_techno( String texto, Long idComentador, Long idArtista) {
		this.Descripcion=texto;
		this.artista= new Artista_techno();
		this.usuario= new Usuario_techno();
		this.artista.setIdArtista(idArtista);
		this.usuario.setIdUsuarioTechno(idComentador);
	}
	public Comentario_techno() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
