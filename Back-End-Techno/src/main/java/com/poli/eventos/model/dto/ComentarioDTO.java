package com.poli.eventos.model.dto;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
public class ComentarioDTO {
	
	private Long idComentario;
	private Long idArtista;
	private long idUsuariocomentador;
	private String descripcion;
	
	public Long getIdComentario() {
		return idComentario;
	}
	public void setIdComentario(Long idComentario) {
		this.idComentario = idComentario;
	}
	public Long getIdArtista() {
		return idArtista;
	}
	public void setIdArtista(Long idArtista) {
		this.idArtista = idArtista;
	}
	public long getIdUsuariocomentador() {
		return idUsuariocomentador;
	}
	public void setIdUsuariocomentador(long idUsuariocomentador) {
		this.idUsuariocomentador = idUsuariocomentador;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
}
