package com.poli.eventos.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="artista_techno")
public class Artista_techno {
	
	public  Artista_techno() {
		// TODO Auto-generated constructor stub
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_Artista")
	private long  idArtista;
	private String nombre;
	private String titulo_perfil;
	private String rutaImagen;
	private String descripcion; 
	
	
	@OneToMany(mappedBy="artista")
	@JsonManagedReference
	private Set<Comentario_techno> comentario =new HashSet<Comentario_techno>(0); 
	

	public Artista_techno(long idArtista, String nombre, String titulo_perfil, String rutaImagen, String descripcion
			//,Set<Comentario_techno> comentario
			) 
	{
		idArtista = idArtista;
		nombre = nombre;
		titulo_perfil = titulo_perfil;
		rutaImagen = rutaImagen;
		descripcion = descripcion;
		//this.comentario = comentario;
	}


	public long getIdArtista() {
		return idArtista;
	}


	public void setIdArtista(long idArtista) {
		this.idArtista = idArtista;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getTitulo_perfil() {
		return titulo_perfil;
	}


	public void setTitulo_perfil(String titulo_perfil) {
		this.titulo_perfil = titulo_perfil;
	}


	public String getRutaImagen() {
		return rutaImagen;
	}


	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Set<Comentario_techno> getComentario() {
		return comentario;
	}


	public void setComentario(Set<Comentario_techno> comentario) {
		this.comentario = comentario;
	}

	public Artista_techno( String nombre, String descripcion) {

		this.nombre=nombre;
		this.descripcion=descripcion;
	}
	
}


