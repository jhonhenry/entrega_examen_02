package servicios;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.poli.eventos.model.dto.ArtistaDTO;
import com.poli.eventos.model.entities.Artista_techno;
import com.poli.eventos.model.entities.Comentario_techno;
import com.poli.eventos.model.entities.Usuario_techno;
import com.poli.eventos.repositories.ArtistaRepository;
import com.poli.eventos.repositories.ComentarioRepository;
import com.poli.eventos.repositories.UsuarioRepository;
@RestController
public class ComentarioController {

	@Autowired
	private ComentarioRepository comentarioRepository;
	@Autowired
	private UsuarioRepository usuarioRepository;

	
	@CrossOrigin
	@RequestMapping(value="/addComentario")
    public String añadirComentarioRepository(@RequestParam String texto ,@RequestParam String nombre ,@RequestParam String claveusuario,@RequestParam String artista ) {
		Usuario_techno UT= usuarioRepository.findByNombreAndContraseña(nombre,claveusuario);
		System.out.println("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
		Comentario_techno comentario_techno= new Comentario_techno( texto, UT.getIdUsuarioTechno() ,  Long.parseLong(artista));
		comentarioRepository.save(comentario_techno);
    	return"";
    }
	@CrossOrigin
	@RequestMapping(value="/Comentarios" )
	public Iterable<Comentario_techno> obtenerComentario_techno (@RequestParam String idArtista) {
		Iterable<Comentario_techno> findAll = comentarioRepository.findByIdArtista_techno(Long.parseLong(idArtista));
		return findAll;
	}
}
