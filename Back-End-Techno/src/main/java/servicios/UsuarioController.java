package servicios;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.poli.eventos.model.dto.UsuarioDTO;
import com.poli.eventos.model.entities.Usuario_techno;
import com.poli.eventos.repositories.UsuarioRepository;
@RestController
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository Usuario_technoRepository;
	
	@RequestMapping(value="/Usuario_technos" )
	public Iterable<Usuario_techno> obtenerUsuario_technos () {
		Iterable<Usuario_techno> findAll = Usuario_technoRepository.findAll();
		return findAll;
		
	}
	
	
	@RequestMapping(value="/añadirUsuario_techno")
    public String añadirUsuario_techno(@RequestParam String nombre,@RequestParam String email,@RequestParam String contraseña ) {
	Usuario_techno Usuario_techno = new Usuario_techno(nombre, email, contraseña);
		Usuario_technoRepository.save(Usuario_techno);
    	return"Usuario_techno Insertado";
    }
	@CrossOrigin
	@RequestMapping(value="/validarLogin")
    public UsuarioDTO validacionLogin(@RequestParam String email,@RequestParam String contraseña ) {
    	Usuario_techno UT= Usuario_technoRepository.findByEmailAndContraseña(email,contraseña);
    	UsuarioDTO UDTO= new UsuarioDTO(UT.getNombre(),UT.getContraseña());
    	return UDTO;
    }

	
}
