package servicios;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.poli.eventos.model.dto.ArtistaDTO;
import com.poli.eventos.model.entities.Artista_techno;
import com.poli.eventos.repositories.ArtistaRepository;


@RestController
public class EventosController {
	
	@Autowired
	private ArtistaRepository artistaRepository;
	@CrossOrigin
	@RequestMapping(value="/eventos" )
	public Set<ArtistaDTO> obtenerUsuarios () {
		Iterable<Artista_techno> findAll = artistaRepository.findAll();
		return ArtistaDTO.Artista_technoAArtistaDTO(findAll);
	}
	@RequestMapping(value="/eventos2" )
	public Iterable<Artista_techno>  obtenerUsuarios2() {
		return artistaRepository.findAll();
	}
		
}
