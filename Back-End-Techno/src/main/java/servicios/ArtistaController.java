package servicios;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.poli.eventos.model.dto.ArtistaDTO;
import com.poli.eventos.model.entities.Artista_techno;
import com.poli.eventos.model.entities.Usuario_techno;
import com.poli.eventos.repositories.ArtistaRepository;
@RestController
public class ArtistaController {


    @RequestMapping("/")
    public String home(){
        return "Hello World!";
    }
    
	@Autowired
	private ArtistaRepository artistaRepository;
	@CrossOrigin
	@RequestMapping(value="/artistas" )
	public Set<ArtistaDTO> obtenerUsuarios () {
		Iterable<Artista_techno> findAll = artistaRepository.findAll();
		return ArtistaDTO.Artista_technoAArtistaDTO(findAll);
	}
	@CrossOrigin
	@RequestMapping(value="/añadirArtista_techno")
    public String añadirUsuario_techno(@RequestParam String nombre,@RequestParam String descripcion ) {
		Artista_techno artista_techno = new Artista_techno( nombre, descripcion);
		artistaRepository.save(artista_techno);
    	return"artista_techno Insertado";
    }		
}
