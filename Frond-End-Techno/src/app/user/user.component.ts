import { Component, OnInit, Input } from '@angular/core';
import {Artista} from '../clases/Artista'
import { Comentario } from '../clases/comentario';
import { CommentaryComponent } from '../commentary/commentary.component';
import {MatDialog} from '@angular/material';
import { UserInformationService } from '../sesion/user-information.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {


  @Input() selectedUser: Artista;
 
  artista:Artista;
  comentario:Comentario;

  constructor(private dialog:MatDialog , private servicio:UserInformationService) {
  }

   onCreate(){
    this.dialog.open(CommentaryComponent);
  }

  ngOnInit() {
  }

}
