import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatMenuModule} from '@angular/material/menu';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { CommentaryComponent } from './commentary/commentary.component';
import {MatCardModule} from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegistryComponent } from './registry/registry.component';
import { UploadComponent} from './upload/upload.component';
import {MatInputModule} from '@angular/material/input';
import { FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ListUserComponent } from './list-user/list-user.component'
import {MatButtonModule} from '@angular/material/button';
import { HomeComponent } from './home/home.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { LoginComponent } from './login/login.component';
import {RouterModule, Route} from '@angular/router';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import { ListCommtaryComponent } from './list-commtary/list-commtary.component';
import { PintarCommentariosComponent } from './pintar-commentarios/pintar-commentarios.component';



const routes: Route[] = [
  {path:'',component: LoginComponent},
  {path:'registry',component: RegistryComponent},
  {path:'home',component: ListUserComponent},
  {path:'comentary',component: CommentaryComponent},
  {path:'list',component: ListCommtaryComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    CommentaryComponent,
    RegistryComponent,
    UploadComponent,
    ListUserComponent,
    HomeComponent,
    LoginComponent,
    ListCommtaryComponent,
    PintarCommentariosComponent,
 

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatInputModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    MatExpansionModule,
    MatDialogModule

  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:  [CommentaryComponent]
})
export class AppModule { }
