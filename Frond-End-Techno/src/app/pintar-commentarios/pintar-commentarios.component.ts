import { Component, OnInit,Input } from '@angular/core';
import { Comentario } from '../clases/comentario';

@Component({
  selector: 'app-pintar-commentarios',
  templateUrl: './pintar-commentarios.component.html',
  styleUrls: ['./pintar-commentarios.component.css']
})
export class PintarCommentariosComponent implements OnInit {

  @Input() selectedComentary:Comentario;

  constructor() { }

  ngOnInit() {
  }

}
