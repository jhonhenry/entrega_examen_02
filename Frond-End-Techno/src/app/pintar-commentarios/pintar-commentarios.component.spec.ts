import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PintarCommentariosComponent } from './pintar-commentarios.component';

describe('PintarCommentariosComponent', () => {
  let component: PintarCommentariosComponent;
  let fixture: ComponentFixture<PintarCommentariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PintarCommentariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PintarCommentariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
