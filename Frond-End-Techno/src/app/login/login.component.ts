import { Component, OnInit } from '@angular/core';
import {UserInformationService} from'../sesion/user-information.service';
import { User } from '../modulos/user';
import {Router, Route} from '@angular/router';
import { ListUserComponent } from '../list-user/list-user.component';

const routes: Route[] = [
  { path: 'home', component: ListUserComponent}

];

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  name:string;
  password:string;

  constructor(private servicio:UserInformationService,private router:Router) {

    
   }

  ngOnInit() {
  }

  enviarInformacion(name, password){
    this.servicio.getData(this.name,this.password).subscribe((user : any)=>{
      if(user.nombre == null){
        alert('Usuario errado ');
      }else{
        localStorage.setItem("nombre", user.nombre);
        localStorage.setItem("clave", user.clave);
        this.router.navigate(['home']);
      }
  });
   

  }
}
