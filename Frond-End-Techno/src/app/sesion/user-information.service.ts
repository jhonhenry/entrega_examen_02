import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from  '@angular/common/http';
import {LoginComponent} from'../login/login.component'
import {User} from '../modulos/user'

import { Observable } from 'rxjs';
import { Artista } from '../clases/artista';
import { Comentario } from '../clases/comentario';

@Injectable({
  providedIn: 'root'
})
export class UserInformationService {
  username:string;
  hobbies:string;

  autenticarUsuario(username:string, password:string){
    if(username=='homersimpson'&& password=='mydonuts'){
      return{
        message:'Autenticacion_Exitosa',
        user:{
          neme:'Homer Simpson',
          hobbie:'Eat donuts and drink beer'
        }
      }
    }
    else{
      return{
        message:'Autenticacion_Fallida',
        user:{
          neme:'',
          hobbie:''
        }
      }
    }
  }

  fillUserInformation(username:string,hobbies:string){
    this.username=username;
    this.hobbies=hobbies;
  }
  logout(){
    this.username='';
    this.hobbies='';
  }

user:User = new User();  

  constructor(private http:HttpClient) {
    console.log('service esta camelloso');

    // Servicio Validador login
  }
  getData(username:string,password:string): Observable <User>{
    return this.http.get<User>('http://localhost:8080/validarLogin?email='+username+'&'+'contraseña='+password);
  }


  getArtistas(): Observable <Artista[]>{
    return this.http.get<Artista[]>('http://localhost:8080/artistas');
  }


  insertArtista (artista: Artista){
    const body = new HttpParams().set('nombre', artista.nombre).set('descripcion', artista.descripcion);
   
    return this.http.post('http://localhost:8080/añadirArtista_techno', body).subscribe();
   }


   insertComentario (comentario: Comentario,nombre: string,claveusuario: string,artista: string){
    console.log('http://localhost:8080/addComentario?texto='+comentario.descripcion+'&nombre='+nombre+''+'&claveusuario='+claveusuario+''+'&artista='+artista+'');
    return this.http.post('http://localhost:8080/addComentario?texto='+comentario.descripcion+'&nombre='+nombre+''+'&claveusuario='+claveusuario+''+'&artista='+artista+'',null).subscribe();

   }
   
   // Metodo nuevo para traer todos los comentarios 

   getComentarios(idArtista): Observable <Comentario[]>{
    return this.http.get<Comentario[]>('http://localhost:8080/Comentarios?idArtista='+idArtista);
  }
   
}
