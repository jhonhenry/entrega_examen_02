import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCommtaryComponent } from './list-commtary.component';

describe('ListCommtaryComponent', () => {
  let component: ListCommtaryComponent;
  let fixture: ComponentFixture<ListCommtaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCommtaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCommtaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
