
import { Component, OnInit, Input } from '@angular/core';
import { Comentario } from '../clases/comentario';
import {MatDialog} from '@angular/material';
import { UserInformationService } from '../sesion/user-information.service';

@Component({
  selector: 'app-list-commtary',
  templateUrl: './list-commtary.component.html',
  styleUrls: ['./list-commtary.component.css']
})

export class ListCommtaryComponent implements OnInit {
  @Input() idArtista:string;

  misComentarios: Array<Comentario>;
  miComentario:Comentario; 

  comentario:Comentario;
    
  constructor(private dialog:MatDialog , private servicio:UserInformationService) {

    this.miComentario=new Comentario;
    this.misComentarios=new Array();
  }

  consultarComentarios(idArtista){
   this.servicio.getComentarios(idArtista).subscribe((comentarios : any)=>{
     this.misComentarios=comentarios;
   });
 }

  ngOnInit() {
        console.log(this.idArtista);

    this.consultarComentarios(this.idArtista+"");

  }

}
