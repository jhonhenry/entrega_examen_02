import { Component, OnInit } from '@angular/core';
import {UploadComponent} from '../upload/upload.component';
import { UserInformationService } from '../sesion/user-information.service';
import {Artista} from '../clases/artista'



@Component({
  selector: 'app-registry',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.css']
})
export class RegistryComponent implements OnInit {

  miArtista:Artista;

  constructor(private servicio:UserInformationService) { 
    this.miArtista= new Artista();
  }

  insertarArtista(){
    this.servicio.insertArtista(this.miArtista);
    this.miArtista= new Artista();
  
  }
  ngOnInit() {
  }

}
