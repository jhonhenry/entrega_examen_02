import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class UploadImageService {

  constructor(private http : HttpClient) { }

  postFile(caption:string,fileToUpload:File){
    const ENDPOINT ='';
    const FORMDATA: FormData = new  FormData();
    FORMDATA.append('Image',fileToUpload,fileToUpload.name);
    FORMDATA.append('ImageCaption',caption);
    return this.http.post(ENDPOINT,FORMDATA);
  }
}
