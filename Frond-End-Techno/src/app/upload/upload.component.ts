import { Component, OnInit } from '@angular/core';
import {UploadImageService} from '../../app/upload/ServiceUpload/upload-image.service'

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css'],
  providers:[ UploadImageService]
})
export class UploadComponent implements OnInit {
  imageUrl:string = "/assets/img/registry.jpg";
  fileToUpload: File =null;

  constructor(private ImageService:UploadImageService) { }

  ngOnInit() {
  }

  handleFileInput(file: FileList){
    this.fileToUpload=file.item(0);
    var reader = new FileReader();
    reader.onload=(event:any)=>{
      this.imageUrl=event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  } 
  onSubmit(Caption,Image){
    this.ImageService.postFile(Caption.value,this.fileToUpload).subscribe(
      data=>{
        console.log('done'); 
      }
    );
  }

}
