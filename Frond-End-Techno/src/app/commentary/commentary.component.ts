import { Component, OnInit, Input } from '@angular/core';
import { UserInformationService } from '../sesion/user-information.service';
import { Comentario } from '../clases/comentario';
import {Router, Route} from '@angular/router';
import { ListUserComponent } from '../list-user/list-user.component';

const routes: Route[] = [
  { path: 'home', component: ListUserComponent}

];

@Component({
  selector: 'app-commentary',
  templateUrl: './commentary.component.html',
  styleUrls: ['./commentary.component.css']
})
export class CommentaryComponent implements OnInit {
  @Input() idArtista:string;

  comentario:Comentario;
  
  constructor( private servicio:UserInformationService,private router:Router) {
    this.comentario= new Comentario();
  }
  
  addComentario(){
    this.servicio.insertComentario(this.comentario,localStorage.getItem("nombre"),localStorage.getItem("clave"),this.idArtista);
    this.comentario.descripcion='';
    this.router.navigate(['home']);
  }
  
  ngOnInit() {

  }

}
