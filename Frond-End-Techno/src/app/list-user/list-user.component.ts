import { Component, OnInit, Input } from '@angular/core';
import {Artista} from '../clases/artista'
import { UserInformationService } from '../sesion/user-information.service';
import { Comentario } from '../clases/comentario';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {
    @Input() selectedUser: Artista;

   misArtistas: Array<Artista>;
   miArtista:Artista;
   comentario:Comentario;

  constructor(private servicio:UserInformationService) {
    

    this.comentario= new Comentario();
    this.miArtista=new Artista;
    this.misArtistas=new Array();

    this.consultarArtistas();
  }

  consultarArtistas(){
    this.servicio.getArtistas().subscribe((artistas : any)=>{
      this.misArtistas=artistas;
    });
  }

   addNewArtista (){
    this.misArtistas.push(this.miArtista);
    this.miArtista=new Artista();
    }


   
  ngOnInit() {
  }

}
